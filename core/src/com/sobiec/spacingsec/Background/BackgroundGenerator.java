package com.sobiec.spacingsec.Background;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class BackgroundGenerator {
	Array<Sprite> sprites; 
	
	public BackgroundGenerator() {
		sprites = new Array<Sprite>();
		
		// Bounds
		int bound_y = Gdx.graphics.getHeight();
		int bound_x = Gdx.graphics.getWidth();
		
		// Background gen
		int x_loops = (int) (bound_x / 256) * 2;
		int y_loops = (int) (bound_y / 256) * 2;
		
		for(int i = 0; i < y_loops; i++) {
			for(int n = 0; n < x_loops; n++) {
				// i = y, n = x
				Sprite temp = new Sprite(new Texture("space2.jpg"));
				temp.setX(n * 256);
				temp.setY(i * 256);
				sprites.add(temp);
			}
		}
		
		// Little deb
		Gdx.app.error("test", "bound_y: " + bound_y);
		Gdx.app.error("test 2", "x_loops/y_loops " + x_loops + "/" + y_loops);
		
		/*for(int i = 0; i < 32; i++) {
			bound_y = i * 256;
			if(i * 256 > bound_y) bound_y = bound_y;
			
			
			Sprite temp = new Sprite(new Texture("space2.jpg"));
			temp.setX(0); temp.setY(bound_y);
			sprites.add(temp);
		}*/
	}
	
	public void draw() {
		SpriteBatch sb = new SpriteBatch();
		sb.begin();
		
		for(Sprite sprite : sprites) {
			sprite.draw(sb);
		}
		
		sb.end();
	}
	
	private boolean addtile(int x, int y) { 
		return true;
	}
}
