package com.sobiec.spacingsec;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.sobiec.spacingsec.Background.BackgroundGenerator;
import com.badlogic.gdx.graphics.GL20;

public class SpacingGame extends ApplicationAdapter implements InputProcessor {
	private Sprite starship;
	private Sprite planet;
	private SpriteBatch sb;
	private OrthographicCamera camera;
	private Viewport viewport;
	private BackgroundGenerator bggen;
	private Hud hud;
	
	@Override
	public void create () {
		starship = new Sprite(new Texture("demo_ship.png"));
		planet = new Sprite(new Texture("s_gasplanet04.png"));
		sb = new SpriteBatch();
		bggen = new BackgroundGenerator();
		hud = new Hud();
		
		planet.setX(100);
		planet.setY(100);
		
		camera = new OrthographicCamera(2f * (Gdx.graphics.getWidth() / Gdx.graphics.getHeight()), 2f);
		viewport = new StretchViewport(500, 500, camera);
		viewport.apply();
		
		Gdx.app.error("test", "planet pos: " + planet.getX() + ", " + planet.getY());
		Gdx.input.setInputProcessor(this);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0f, 0f, 0f, 1);
	    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	   
	    planet.rotate(0.06f);
	    
		camera.update();
		bggen.draw();		// t�o gry
	    
		sb.begin();
		starship.draw(sb);
		planet.draw(sb);
		sb.end();
		
		hud.draw();
	}
	
	@Override
	public void resize(int width, int height) {
		float aspectRatio = (float) width / (float) height;
        camera = new OrthographicCamera(2f * aspectRatio, 2f);
        viewport.update(width,height);
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub

		Gdx.app.error("touchDragged", "ok (" + screenX + ", " + screenY + ")");
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
