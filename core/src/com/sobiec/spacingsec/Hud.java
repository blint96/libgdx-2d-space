package com.sobiec.spacingsec;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

// (350 / base_x) * current_x
public class Hud {
	private static float base_x = 1366;
	private static float base_y = 768;
	private float current_x;
	private float current_y;
	private BitmapFont  font;
	private Sprite resourcesBackground;
	
	public Hud() {
		current_x = Gdx.graphics.getWidth();
		current_y = Gdx.graphics.getHeight();
		font = new BitmapFont();
		resourcesBackground = new Sprite(new Texture("resources/res_bg.png"));
		resourcesBackground.setSize(this.toRelativeX(1000), this.toRelativeX(100));
		resourcesBackground.setX(this.toRelativeX(183)); resourcesBackground.setY(this.toRelativeY(668));
	}
	
	public void draw() {
		SpriteBatch batch = new SpriteBatch();
		batch.begin();
		font.setColor(Color.ORANGE);
		font.draw(batch, "test str", 100, this.toRelativeY(700));
		resourcesBackground.draw(batch);
		batch.end();
	}
	
	private int toRelativeY(int value) {
		float relative_y = (value / base_y) * current_y;
		return (int)relative_y;
	}
	
	private int toRelativeX(int value) {
		float relative_x = (value / base_x) * current_x;
		return (int)relative_x;
	}
}
