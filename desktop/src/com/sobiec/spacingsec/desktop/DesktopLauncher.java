package com.sobiec.spacingsec.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.sobiec.spacingsec.SpacingGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.fullscreen = true;
		config.width = 0;
		config.height = 0;
		config.samples = 4;
		new LwjglApplication(new SpacingGame(), config);
	}
}
